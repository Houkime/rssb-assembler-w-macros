### Prereq:

* make
* automake
* autoconf
* C compiler

### Compilation steps

* autoreconf --install
* autoconf
* ./configure
* make

Executable will be src/rssb

### Usage

rssb <path/to/assemblyfile> > output.file

it can also emulate an rssb machine if activated through source (no cli flags yet, sorry)